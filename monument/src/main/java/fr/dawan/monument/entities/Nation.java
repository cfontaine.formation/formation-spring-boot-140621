package fr.dawan.monument.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(length = 100, nullable = false)
    private String nom;
    
    @OneToMany(mappedBy = "nation")
    private Set<Monument> monuments;

    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nation [nom=" + nom + "]";
    }
}
