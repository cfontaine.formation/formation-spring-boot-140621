package fr.dawan.monument.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="etiquettes")
public class Etiquette extends AbstractEntity {
    
    private static final long serialVersionUID = 1L;
    
    @Column(nullable = false)
    private String nom;

    
    public Etiquette() {
    }

    public Etiquette(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Etiquette [nom=" + nom + "]";
    }
}
