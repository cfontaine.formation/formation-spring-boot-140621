package fr.dawan.monument.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="monuments")
public class Monument extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    private String nom;
    
    @Column(nullable = false)
    private int anneeConstruction;
    
    @OneToOne
    private Coordonee coordonee;
    
    @ManyToOne
    private Nation nation;
    
    @ManyToMany
    private Set<Etiquette> etiquettes;

    public Monument() {
    }

    public Monument(String nom, int anneeConstruction, Coordonee coordonee, Nation nation) {
        this.nom = nom;
        this.anneeConstruction = anneeConstruction;
        this.coordonee = coordonee;
        this.nation = nation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneeConstruction() {
        return anneeConstruction;
    }

    public void setAnneeConstruction(int anneeConstruction) {
        this.anneeConstruction = anneeConstruction;
    }

    public Coordonee getCoordonee() {
        return coordonee;
    }

    public void setCoordonee(Coordonee coordonee) {
        this.coordonee = coordonee;
    }

    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public Set<Etiquette> getEtiquettes() {
        return etiquettes;
    }

    public void setEtiquettes(Set<Etiquette> etiquettes) {
        this.etiquettes = etiquettes;
    }

    @Override
    public String toString() {
        return "Monument [nom=" + nom + ", anneeConstruction=" + anneeConstruction + ", coordonee=" + coordonee + "]";
    }
    
}
