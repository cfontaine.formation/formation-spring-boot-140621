package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Produit;


@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

    List<Produit>findByPrixLessThan(double prix);
    
    List<Produit> findByPrixBetweenOrderByPrixAsc(double prixMin, double prixMax);
    
    List<Produit>findByPrixGreaterThanAndDescriptionLike(double prix,String description);
    
    @Query(value="SELECT p FROM Produit p JOIN p.marque m WHERE m.nom=:nomMarque")
    List<Produit>findByNomMarque(@Param("nomMarque")String nomMarque);
   
    @Query(nativeQuery = true, value="SELECT * FROM produits JOIN marques ON Produits.marque_id=marques.id WHERE marques.nom=:nomMarque")
    List<Produit>findByNomMarqueSQL(@Param("nomMarque")String nomMarque);
    
    Page<Produit> findByPrixGreaterThan(double prix,Pageable page);
}
