package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Produit;

@Repository
public interface MarqueRepository extends JpaRepository<Produit, Long> 
{

}
