package fr.dawan.springboot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc") 
public class TestMvcController {
    
    @GetMapping("/hello/{nom}")
    public String hello(@PathVariable String nom, Model model) {
        model.addAttribute("nom", nom);
        return "hello";
    }
}
