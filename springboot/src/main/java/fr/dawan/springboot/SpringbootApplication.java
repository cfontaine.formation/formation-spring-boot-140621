package fr.dawan.springboot;

import java.io.PrintStream;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SpringbootApplication.class);
        // app.setAddCommandLineProperties(false);
//        app.setBanner(new Banner() {
//            
//            @Override
//            public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
//                out.println("Sprinboot");
//                
//            }
//        });
        app.run(args);

        // SpringApplication.run(SpringbootApplication.class, args);
    }

}
