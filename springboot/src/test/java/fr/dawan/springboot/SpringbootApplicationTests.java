package fr.dawan.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.repositories.ProduitRepository;

@SpringBootTest
class SpringbootApplicationTests {

    @Autowired
    ProduitRepository produitRepository;

    @Test
	void contextLoads() {
//	    List<Produit> lstProd=produitRepository.findByPrixLessThan(200.0);
//	    for(Produit p:lstProd) {
//	        System.out.println(p);
//	    }
        Page<Produit> page=produitRepository.findByPrixGreaterThan(30.0,PageRequest.of(0,2));
        System.out.println(page.getNumber());
        System.out.println(page.getTotalPages());
        System.out.println(page.getTotalElements());
        for(Produit p:page.getContent()){
            System.out.println(p);
        }
	}

}
